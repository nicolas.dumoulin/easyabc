# EasyABC

EasyABC: R Package for Efficient Approximate Bayesian Computation Sampling Schemes 

The project is hosted on [R-Forge : https://r-forge.r-project.org/projects/easyabc/](https://r-forge.r-project.org/projects/easyabc/)
